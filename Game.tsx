import * as React from "react";
import Player from "./Components/Player";
import Box from "./Components/Box";
import {GameState} from "./State";
import * as PixiFibre from "react-pixi-fiber"
import {Component} from "react";
import {CollissionTreeDebugger} from "./Collisions/CollisionTreeDebugger";
import {Scene} from "./GameObject/Scene";

const {Stage} = PixiFibre;
const {Component} = React;

export default class Game extends Component {
  private game: GameState;
  private scene: Scene;

  constructor(...params) {
    super(...params);

    this.game = {
      keys: [],
      left: false,
      right: false,
      up: false,
      down: false,
      shoot: false,
      player: null,
      collidesLeft: false,
      collidesLeftWith: false,
      collidesRight: false,
      collidesRightWith: false,
      collidesUp: false,
      collidesUpWith: false,
      collidesDown: false,
      collidesDownWith: false,
      objects: [],
    };
    this.scene = new Scene();
    this.scene.addChild(<Box key={'b1'} x={40} y={20} w={20} h={20}/>);

    this.animate = this.animate.bind(this)
  }

  componentDidMount(): void {
    window.addEventListener("keydown", e => {
      this.game = {
        ...this.game,
        keys: {
          ...this.game.keys,
          [e.which]: true,
        }
      }
    });

    window.addEventListener("keyup", e => {
      this.game = {
        ...this.game,
        keys: {
          ...this.game.keys,
          [e.which]: false,
        }
      }
    });

    this.animate()
  }

  componentDidCatch(error, info): void {
    console.error(error, info);
  }

  animate(): void {
    requestAnimationFrame(this.animate);
    if (this.game.player) {
      this.game.player.animate(this.game);
    }

    this.game.objects.forEach(
      object => object.animate(this.game)
    )
  }

  private randomItems() {
    let nodes = [];
    for (let i = 0; i < 200; i++) {
      nodes.push(<Box key={i} x={this.getRandomInt(window.innerWidth)} y={this.getRandomInt(window.innerHeight)} w={10}
                      h={10}/>);
    }
    return nodes;
  }

  private getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
  }

  render(): Component {
    this.game.player = undefined;
    this.game.objects = [];

    return (
      <Stage
        width={this.props.w}
        height={this.props.h}
        options={{backgroundColor: 0x000000}}
      >
        <Player gravity={1} x={this.props.w / 2} y={this.props.h / 2} ref={player => this.game.player = player}/>
        <Box x={this.props.w / 2} y={this.props.h - 200} w={100} h={100}/>
        <Box x={0} y={this.props.h - 200} w={100} h={100}/>
        <Box x={100} y={this.props.h - 200} w={100} h={100}/>
        <Box x={200} y={this.props.h - 200} w={100} h={100}/>
        <Box x={300} y={this.props.h - 200} w={100} h={100}/>
        <Box x={400} y={this.props.h - 200} w={100} h={100}/>
        <Box x={500} y={this.props.h - 200} w={100} h={100}/>
        <Box x={600} y={this.props.h - 200} w={100} h={100}/>
        <Box x={700} y={this.props.h - 200} w={100} h={100}/>
        <Box x={800} y={this.props.h - 200} w={100} h={100}/>
        <Box x={900} y={this.props.h - 200} w={100} h={100}/>
        <Box x={1000} y={this.props.h - 200} w={100} h={100}/>
        <Box x={1100} y={this.props.h - 200} w={100} h={100}/>
        {this.scene.getGameObjects()}
        {this.randomItems()}
        <CollissionTreeDebugger/>
      </Stage>
    );
  }
}
