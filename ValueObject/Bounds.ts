export interface Bounds {
  x: number;
  y: number;
  h: number;
  w: number;
}
