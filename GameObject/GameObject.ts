import {GameState} from "../State";
import {Component} from "react";

export default class GameObject extends Component{
  animate(state: GameState): void;

  render();
}
