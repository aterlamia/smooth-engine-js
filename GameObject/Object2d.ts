export interface Object2d {
  x():number;
  y():number;
  height(): number;
  width(): number;
}
