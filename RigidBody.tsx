import * as React from "react";
import {GameState} from "./State";
import {Vector2} from "./ValueObject/Vector2";
import GameObject from "./GameObject/GameObject";
import {CollisionRegistry} from "./Collisions/CollisionRegistry";

const {Component} = React;

export default class RigidBody extends Component implements GameObject {
  private collisonRegistry: CollisionRegistry;
  get y(): number {
    return this._y;
  }
  get x(): number {
    return this._x;
  }
  protected _x: number;
  protected _y: number;
  private state: Vector2;
  private props: any;
  private gravity: Number;

  constructor(...params) {
    super(...params);

    this._x = this.props.x;
    this._y = this.props.y;
    this.gravity = this.props.gravity;
    this.state = {
      x: this.props.x,
      y: this.props.y,
    };

    this.collisonRegistry;
  }

  componentWillReceiveProps(props) {
    this._x = this.props.x;
    this._y = this.props.y;
  }

  animate(state: GameState): void {
    this._y = this._y + this.gravity;

  }


  render(): any {
    console.log('Implement your own render');
  }
}
