import {Bounds} from "../ValueObject/Bounds";
import {CollisionObject} from "./CollisionObjects";

class CollisionTreeNode {
    private element: HTMLDivElement;

    get treeSize(): Bounds {
        return this._treeSize;
    }

    get objects(): CollisionObject[] {
        return this._objects;
    }

    get nodes(): CollisionTreeNode[] {
        return this._nodes;
    }

    private depth: number;
    private maxNodes: number;
    private maxDepth: number;
    private _treeSize: Bounds;
    private _nodes: CollisionTreeNode[];

    private _objects: CollisionObject[];

    constructor(maxNodes: number, maxDepth: number, treeSize: Bounds, depth: number) {
        this.depth = depth;
        this.maxNodes = maxNodes;
        this.maxDepth = maxDepth;
        this._treeSize = treeSize;
        this._nodes = [];
        this._objects = [];
    }

    insert(collisionObj: CollisionObject): void {
        let quad;
        let i = 0;

        // When we have quadrants already find the correct quadrants and add it there.SS
        if (this.nodes.length > 0) {
            quad = this.getQuad(collisionObj);

            if (quad !== -1) {
                this.nodes[quad].insert(collisionObj);
                return;
            }
        }

        this._objects.push(collisionObj);
        if (this._objects.length > this.maxNodes && this.depth <= this.maxDepth) {

            if (this._nodes.length !== 4) {
                this.split();
            }

            while (i < this.objects.length) {

                quad = this.getQuad(this.objects[i]);

                if (quad !== -1) {
                    this.nodes[quad].insert(this.objects.splice(i, 1)[0]);
                } else {
                    i++;
                }
            }

        }
    }

    getQuad(collisionObj: CollisionObject): number {
        let quad = -1;
        let middleH = this._treeSize.y + (this._treeSize.h / 2);
        let middleV = this._treeSize.x + (this._treeSize.w / 2);

        let topQuadrant = (collisionObj.getBoundingBox().y < middleH && collisionObj.getBoundingBox().y + collisionObj.getBoundingBox().h < middleH);
        let bottomQuadrant = (collisionObj.getBoundingBox().y > middleH);

        if (collisionObj.getBoundingBox().x < middleV && collisionObj.getBoundingBox().x + collisionObj.getBoundingBox().w < middleV) {
            if (topQuadrant) {
                quad = 0;
            } else if (bottomQuadrant) {
                quad = 2;
            }
        } else if (collisionObj.getBoundingBox().x > middleV) {
            if (topQuadrant) {
                quad = 1;
            } else if (bottomQuadrant) {
                quad = 3;
            }
        }

        return quad;
    }

    private split() {

        let quadrantWidth = Math.round(this._treeSize.w / 2);
        let quadrantHeight = Math.round(this._treeSize.h / 2);
        let x = Math.round(this._treeSize.x);
        let y = Math.round(this._treeSize.y);

        this._nodes.push(new CollisionTreeNode(
            this.maxNodes,
            this.maxDepth,
            {
                x: x,
                y: y,
                w: quadrantWidth,
                h: quadrantHeight
            }, this.depth + 1)
        );

        this._nodes.push(new CollisionTreeNode(
            this.maxNodes,
            this.maxDepth,
            {
                x: x + quadrantWidth,
                y: y,
                w: quadrantWidth,
                h: quadrantHeight
            }, this.depth + 1)
        );

        this._nodes.push(new CollisionTreeNode(
            this.maxNodes,
            this.maxDepth,
            {
                x: x,
                y: y + quadrantHeight,
                w: quadrantWidth,
                h: quadrantHeight
            }, this.depth + 1)
        );

        this._nodes.push(new CollisionTreeNode(
            this.maxNodes,
            this.maxDepth,
            {
                x: x + quadrantWidth,
                y: y + quadrantHeight,
                w: quadrantWidth,
                h: quadrantHeight
            }, this.depth + 1)
        );
    }

    getObjectsInQuadrant = function (collisionObj: CollisionObject) {
        const index = this.getQuad(collisionObj);

        let foundObjects = this._objects;

        // Do we actually have nodes, if not we don't need to check
        if (this._nodes.length > 0) {
            if (index !== -1) {
                foundObjects = foundObjects.concat(this._nodes[index].getObjectsInQuadrant(collisionObj));
            } else {
                for (let i = 0; i < this._nodes.length; i = i + 1) {
                    foundObjects = [...foundObjects, ...this._nodes[i].getObjectsInQuadrant(collisionObj)];
                }
            }
        }

        return foundObjects;
    };

    bindElement(element: HTMLDivElement) {
        this.element = element;

    }
}

export class CollisionTree {
    get treeNode(): CollisionTreeNode {
        return this._treeNode;
    }

    get treeSize(): Bounds {
        return this._treeSize;
    }

    private depth: number;
    private maxNodes: number;
    private maxDepth: number;
    private _treeSize: Bounds;
    private _treeNode: CollisionTreeNode;

    constructor(maxNodes: number, maxDepth: number, treeSize: Bounds, depth: number) {
        this.depth = depth;
        this.maxNodes = maxNodes;
        this.maxDepth = maxDepth;
        this._treeSize = treeSize;
        this._treeNode = new CollisionTreeNode(maxNodes, maxDepth, treeSize, depth);
    }

    insert(collisionObj: CollisionObject): void {
        this._treeNode.insert(collisionObj);
    }

    getObjectsInQuadrant = function (collisionObj: CollisionObject) {
        return this._treeNode.getObjectsInQuadrant(collisionObj);
    }
}

