import {Bounds} from "../ValueObject/Bounds";

export interface CollisionObject {
  collides(): boolean;

  x(): number;

  y(): number;

  getBoundingBox(): Bounds;
}

export class BoxCollision implements CollisionObject {
  private x: number;
  private y: number;
  private w: number;
  private h: number;

  constructor(x: number, y: number, w: number, h: number) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  collides(): boolean {
    return undefined;
  }

  getBoundingBox(): Bounds {
    return {x: this.x, y: this.y, w: this.w, h: this.h};
  }

  x(): number {
    return this.x;
  }

  y(): number {
    return this.y;
  }
}
