import * as React from "react";
import * as PixiFibre from "react-pixi-fiber"
import * as PIXI from "pixi.js"
import {GameState} from "../State";
import {Vector2} from "../ValueObject/Vector2";
import RigidBody from "../RigidBody";
import {CollisionRegistry} from "../Collisions/CollisionRegistry";
import {BoxCollision} from "../Collisions/CollisionObjects";

const walkSprite = require('../Assets/Images/walkSeparated.png');
const {Sprite} = PixiFibre;

export default class Player extends RigidBody {
  private state: Vector2;
  private velocity: Vector2;
  private limit: Vector2;
  private acceleration: Vector2;
  private friction: Vector2;
  private base: PIXI.Texture;
  private rec: PIXI.Rectangle;
  private idle: PIXI.Texture;
  private delta: number;
  private walkCycle: PIXI.Texture[];
  private animationSpeed: number;
  private frames: number;
  private props: any;

  constructor(...params) {
    super(...params);
    this.delta = 0;

    this.velocity = {
      x: 0,
      y: 0,
    };

    this.limit = {
      x: 8,
      y: 8,
    };

    this.acceleration = {
      x: 2,
      y: 2,
    };

    this.friction = {
      x: 0.9,
      y: 0.9,
    };


    this.base = PIXI.Texture.fromImage(walkSprite);
    this.rec = new PIXI.Rectangle(0, 0, 510, 887);

    let frame1 = new PIXI.Rectangle(0, 0, 500, 887);
    let frame2 = new PIXI.Rectangle(500, 0, 500, 887);
    let frame3 = new PIXI.Rectangle(1000, 0, 500, 887);
    let frame4 = new PIXI.Rectangle(1500, 0, 500, 887);
    let frame5 = new PIXI.Rectangle(2000, 0, 500, 887);
    let frame6 = new PIXI.Rectangle(2500, 0, 500, 887);
    let frame7 = new PIXI.Rectangle(3000, 0, 500, 887);
    this.walkCycle = [
      new PIXI.Texture(this.base.baseTexture, frame1, frame1, frame1),
      new PIXI.Texture(this.base.baseTexture, frame2, frame1, frame1),
      new PIXI.Texture(this.base.baseTexture, frame3, frame1, frame1),
      new PIXI.Texture(this.base.baseTexture, frame4, frame1, frame1),
      new PIXI.Texture(this.base.baseTexture, frame5, frame1, frame1),
      new PIXI.Texture(this.base.baseTexture, frame6, frame1, frame1),
      new PIXI.Texture(this.base.baseTexture, frame7, frame1, frame1),
    ];

    this.frames = this.walkCycle.length;
    this.animationSpeed = 0.2;
    this.idle = new PIXI.Texture(this.base.baseTexture, this.rec, this.rec, this.rec);

    console.table(params);
    CollisionRegistry.getCollsionTree().insert(new BoxCollision(params.x, params.y,125, 220));
  }

  animate(state: GameState): void {
    super.animate(state);

    this.delta+= this.animationSpeed;

    let currentFrame = Math.floor(this.delta) % this.frames;
    this.idle = this.walkCycle[currentFrame];
    if (state.keys[65] && state.keys[68]) {
      // do nothing
    }
    else if (state.keys[65]) {
      this.velocity.x = Math.max(
        this.velocity.x - this.acceleration.x,
        this.limit.x * -1,
      )
    }
    else if (state.keys[68]) {
      this.velocity.x = Math.min(
        this.velocity.x + this.acceleration.x,
        this.limit.x,
      )
    }

    if (state.keys[87] && state.keys[83]) {
      // do nothing
    }
    else if (state.keys[87]) {
      this.velocity.y = Math.max(
        this.velocity.y - this.acceleration.y,
        this.limit.y * -1,
      )
    }
    else if (state.keys[83]) {
      this.velocity.y = Math.min(
        this.velocity.y + this.acceleration.y,
        this.limit.y,
      )
    }

    this.velocity.x *= this.friction.x;
    this._x += this.velocity.x;

    this.velocity.y *= this.friction.y;
    this._y += this.velocity.y;

    this.setState({
      x: this._x,
      y: this._y,
    });
  }

  render(): any {
    const {x, y} = this.state;

    return (
      <Sprite
        texture={this.idle}
        x={x}
        y={y}
        width={125}
        height={222}
      />
    )
  }
}
