import * as React from "react";
import * as PixiFibre from "react-pixi-fiber"
import * as PIXI from "pixi.js"
import {BoxCollision} from "../Collisions/CollisionObjects";
import {CollisionRegistry} from "../Collisions/CollisionRegistry";
import GameObject from "../GameObject/GameObject";

const boxSprite = require('../Assets/Images/Ground.png');
const {Sprite} = PixiFibre;
const {Texture} = PIXI;
const {Component} = React;

export default class Box extends GameObject {
  constructor(...params) {
    super(...params);
    const {x, y, w, h} = this.props;

    // ...start it a bit higher to account for motion
    this.y = y - 15;
    this.x = x;

    console.log(x, y, w, h);
    CollisionRegistry.getCollsionTree().insert(new BoxCollision(x, y, w, h));
  }

  animate(state) {
  }

  render() {
    const {x, y, w, h} = this.props;

    return (
      <Sprite
        texture={Texture.from(boxSprite)}
        x={x}
        y={y}
        width={w}
        height={h}
      />
    )
  }
}
